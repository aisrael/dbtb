# frozen_string_literal: true

# The base ApplicationRecord
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
